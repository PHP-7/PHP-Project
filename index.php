<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>This is PHP Coding </title>
    </head>
    <body>
        <?php
       
        //This is a single line coment;
        
        /*This is a multiple-lines comment block
        that spans over multiple
        lines*/
        echo " <b> <i> <u> <font color= red size=10>Hello in PHP </b> </i> </u> </font> <br>" ;
        //declaring integer data type
        $int_val=32767;
        echo $int_val."<br>";
        
        //declaring string data type
        $str="Towfiq Rahman \n";
        echo $str."<br>";
        
        /*Simple assignment operator, Assigns values from right side operands 
         * to left side operand 
         */
        $a=50;
        $b=50;
        $c=$a+$b;
        
        echo "total summation is=". $c."<br>";
        
        // Arithmetic Operator
        $mul= $a*$b;
        
        echo "multiplication is=". $mul."<br>";
        
        //String Concatenation
        
        $str="My First PHP";
        
        echo $str." "."Project". "<br>";
        
        //Comparison Operator
        
        $a=10;
        $b=5;
            if($a>$b)
                echo "a is greater than b". "<br>";
            else echo "wrong statement";
            
         //Logical Operators
        $a=5;
        $b=10;
            if ($a==5 && $b==15)
                echo"statement is true\n";
            else echo "statement is wrong\n". "<br>";
                
        //Increament operator
            $a=10;
            if ($a==10)
                $a++;
            echo "increamented value is =". $a."<br>";
            
        //Decreament operator
            $a=10;
            if ($a==10)
                $a--;
            echo "increamented value is =". $a."<br>";
            
        //Defining Constant in php
            define("max",150);
            echo "constant value is=". max;
        ?>
        
    </body>
</html>
